<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UtilityProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public static function arrayKeyBy($array, $key, $multiple = false, $secondKey = '')
    {
        $tmpArray = [];

        if($multiple) {
            foreach($array as $element) {
                if(!empty($element[$key])) {
                    $tmpArray[$element[$key]][$element[$secondKey]][] = $element;
                }
            }
        } else {
            foreach($array as $element) {
                if(!empty($element[$key])) {
                    $tmpArray[$element[$key]] = $element;
                }
            }
        }

        return $tmpArray;
    }

    public static function addBoxSufix($sum)
    {
        if($sum == 1) {
            return $sum . ' skrzynka';
        }
        else if($sum > 1 && $sum < 5) {
            return $sum . ' skrzynki';
        } else {
            return $sum . ' skrzynek';
        }
    }
}
