<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxImport extends Model
{
    protected $fillable = [
        'userId', 'count', 'password',
    ];
}
