<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function api(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if(!empty($email) && !empty($password)) {
            $user = User::where('email', '=', $email)->first();

            if(!empty($user)) {
                if(Hash::check($password, $user->password)) {
                    return response()->json(['status'=>'success', 'code' => 0]);
                }
            }
        }

        return response()->json(['status'=>'error', 'code' => 1]);
    }
}
