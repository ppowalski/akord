<?php
/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 29.02.2020
 * Time: 16:57
 */

namespace App\Http\Controllers;

use App\Box;
use Illuminate\Http\Request;

class SynchronizeController extends Controller
{
    public function getBoxes(Request $request)
    {
        $token = $request->input('token');

        if($token != "AHFJEN123123NFHFJENFndfdsfsfNJFENN38r83fnsfOOmdamsdPP99w") {
            return response()->json(['status' => 'error']);
        }

        $box = new Box();
        $data = $box::where('deleted', 0)->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }
}