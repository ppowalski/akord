<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;
use App\EmployeesBoxs;
use App\Providers\UtilityProvider;
use DateInterval;
use DateTime;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(string $id, Request $request)
    {
        $from = $request->input('date-from', date('Y-m-01'));
        $to = $request->input('date-to', date('Y-m-d'));

        // Get employee boxes
        $employeesBoxs = new EmployeesBoxs();
        $employeesBoxsData = $employeesBoxs::where('employees_boxs.deleted', 0)
            ->leftJoin('boxes', 'employees_boxs.box_fkey', '=', 'boxes.id')
            ->where('employees_boxs.date', '>=', $from)
            ->where('employees_boxs.date', '<=', $to)
            ->where('employees_boxs.employee_fkey', '=', $id)
            ->get();

        // Get employee data
        $employee = Employees::where('deleted', 0)->where('id', $id)->first();
    
        // Generate Period
        $dateTimeTo = new DateTime($to);
        $dateTimeTo->add(new DateInterval('P1D'));

        $period = new \DatePeriod(
            new DateTime($from),
            new DateInterval('P1D'),
            $dateTimeTo
        );

        $dayNames = array(
            0 => 'Niedziela',
            1 => 'Poniedziałek', 
            2 => 'Wtorek', 
            3 => 'Środa', 
            4 => 'Czwartek', 
            5 => 'Piątek', 
            6 => 'Sobota', 
         );

        $employeesBoxsData = UtilityProvider::arrayKeyBy($employeesBoxsData, 'name', true, 'date');
   
        $header = [];
        $sumDays = [
            'sum' => [
                'price' => 0,
                'count' => 0,
                'kg' => 0,
            ],
        ];
        foreach($period as $day) {
            $date = $day->format('Y-m-d');
            $dayNumber = $day->format('w');

            $header[$date] = [
                'date' => $date,
                'dayName' => $dayNames[$dayNumber],
            ];

            $sumDays[$date] = [
                'price' => 0,
                'count' => 0,
                'kg' => 0,
            ];
        }

        foreach($employeesBoxsData as $box => $days) {
            $oneDay = current(current($days));

            $globalSum = [
                'price' => 0,
                'count' => 0,
                'kg' => 0,
            ];

            foreach($days as $key => $day) {
                $sum = [
                    'price' => 0,
                    'count' => 0,
                    'kg' => 0,
                ];

                foreach($day as $harvest) {
                    $sum['price'] += ($harvest['price'] * $harvest['weight'] * $harvest['count']);
                    $sum['count'] += $harvest['count'];
                    $sum['kg'] += ($harvest['weight'] * $harvest['count']);

                    $globalSum['price'] += ($harvest['price'] * $harvest['weight'] * $harvest['count']);
                    $globalSum['count'] += $harvest['count'];
                    $globalSum['kg'] += ($harvest['weight'] * $harvest['count']);

                    $sumDays[$key]['price'] += ($harvest['price'] * $harvest['weight'] * $harvest['count']);
                    $sumDays[$key]['count'] += $harvest['count'];
                    $sumDays[$key]['kg'] += ($harvest['weight'] * $harvest['count']);

                    $sumDays['sum']['price'] += ($harvest['price'] * $harvest['weight'] * $harvest['count']);
                    $sumDays['sum']['count'] += $harvest['count'];
                    $sumDays['sum']['kg'] += ($harvest['weight'] * $harvest['count']);
                }

                $sum['count'] = UtilityProvider::addBoxSufix($sum['count']);

                $employeesBoxsData[$box][$key] = $sum;
            }


            $globalSum['count'] = UtilityProvider::addBoxSufix($globalSum['count']);
            $employeesBoxsData[$box]['sum'] = $globalSum;
            $employeesBoxsData[$box]['name'] = $oneDay['name'] . ' (' . number_format($oneDay['price'], 2) . ' PLN)';
        }

        foreach($sumDays as $key => $day) {
            $sumDays[$key]['count'] = UtilityProvider::addBoxSufix($day['count']);
        }

        if(!empty($employee)) {
            return view('employee.index', ['from' => $from, 'to' => $to, 'employee' => $employee, 'header' => $header, 'data' => $employeesBoxsData, 'footer' => $sumDays]);
        } else {
            return back();
        }
    }
}
