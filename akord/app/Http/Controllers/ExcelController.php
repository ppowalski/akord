<?php

namespace App\Http\Controllers;

use App\Backup;
use App\Box;
use App\BoxImport;
use App\Employees;
use App\EmployeesBoxs;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $box = new Box();
        $data = $box::where('deleted', 0)
            ->get();

        $backupData = Backup::orderBy('backup.id', 'desc')
            ->leftJoin('boxes', 'backup.box_fkey', '=', 'boxes.id')
            ->select('backup.*', 'boxes.name as box_name')
            ->get();

        return view('excel.index', ['data' => $data, 'backupData' => $backupData]);
    }

    /**
     * @param Request $request
     * @return $this|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'day' => 'required',
            'box' => 'required',
            'file'  => 'required|mimes:xls,xlsx'
        ]);

        $day = $request->input('day');
        $day = Carbon::parse($day)->format('Y-m-d');

        $boxId = $request->input('box');

        $path1 = $request->file('file')->store('temp');
        $fileName = $request->file('file')->getClientOriginalName();
        $path = storage_path('app').'/'.$path1;

        $data = Excel::toArray(new BoxImport, $path);

        $rows = [];
        if(!empty($data[0])) {
            foreach($data[0] as $id => $row){
                if($id === 0) {
                    continue;
                } else {
                    if(!empty($rows[$row[0]])) {
                        if(!empty($row[1])) {
                            $rows[$row[0]] += $row[1];
                        } else {
                            $rows[$row[0]] += 1;
                        }
                    } else {
                        if(!empty($row[1])) {
                            $rows[$row[0]] = $row[1];
                        } else {
                            $rows[$row[0]] = 1;
                        }
                    }
                }
            }

            if(!empty($rows)) {
                $data = Employees::where('deleted', 0)->get();

                $employees = [];
                foreach($data as $emp) {
                    $employees[$emp->code] = $emp->id;
                }

                $backup = new Backup();
                $backup->name = $fileName;
                $backup->date = $day;
                $backup->box_fkey = $boxId;
                $backup->save();

                foreach($rows as $code => $count) {
                    if(!empty($employees[$code])) {
                        $boxes = new EmployeesBoxs();

                        $boxes->employee_fkey = $employees[$code];
                        $boxes->box_fkey = $boxId;
                        $boxes->count = $count;
                        $boxes->date = $day;
                        $boxes->backup_fkey = $backup->id;
                        $boxes->save();
                    }
                }

                return back()->with('success', 'Dodano Skrzynki');
            }
        }

        return back()->with('error', 'Nie dodano skrzynke');
    }


    public function revert(Request $request)
    {
        $id = $request->input('revert-id', null);

        if(!empty($id)) {
            Backup::where('id', $id)->update(['deleted' => false]);
            EmployeesBoxs::where('backup_fkey', $id)->update(['deleted' => false]);

            return back();
        }
    }

    public function deleteImport(Request $request)
    {
        $id = $request->input('delete-id', null);

        if(!empty($id)) {
            Backup::where('id', $id)->update(['deleted' => true]);
            EmployeesBoxs::where('backup_fkey', $id)->update(['deleted' => true]);

            return back();
        }
    }

    public function downloadExcel(Request $request)
    {
        $workerName = $request->input('workerName', rand(1, 9999));
        $table = $request->input('table', '');

        $table = urldecode($table);

        $fileName = $workerName . ".xls";

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($table);


        $nCols = 1000;
        foreach (range(0, $nCols) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($fileName); 

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileName));
        flush();
        readfile($fileName);
        unlink($fileName);
        exit;
    }
}
