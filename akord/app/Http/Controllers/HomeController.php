<?php

namespace App\Http\Controllers;

use App\Box;
use App\Employees;
use App\EmployeesBoxs;
use App\Providers\UtilityProvider;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $header = ['Pracownik'];
        $footer = [];
        $result = [];
        $from = $request->input('date-from', date('Y-m-01'));
        $to = $request->input('date-to', date('Y-m-d'));

        if(strtotime($from) && strtotime($to) && strtotime($from) <= strtotime($to)) {

            // Get employees boxes
            $employeesBoxs = new EmployeesBoxs();
            $employeesBoxsData = $employeesBoxs::where('employees_boxs.deleted', 0)
                ->where('employees_boxs.date', '>=', $from)
                ->where('employees_boxs.date', '<=', $to)
                ->get();

            $employeesBoxsData = UtilityProvider::arrayKeyBy($employeesBoxsData, 'employee_fkey', true, 'date');

            // Get boxes
            $boxes = new Box();
            $boxData = $boxes::where('deleted', 0)->get();

            $boxData = UtilityProvider::arrayKeyBy($boxData, 'id');

            // Get Employees
            $employees = new Employees();
            $employeesData = $employees::where('deleted', 0)->get();

            // Generate Period
            $dateTimeTo = new DateTime($to);
            $dateTimeTo->add(new DateInterval('P1D'));

            $period = new \DatePeriod(
                new DateTime($from),
                new DateInterval('P1D'),
                $dateTimeTo
            );

            $dayNames = array(
                0 => 'Niedziela',
                1 => 'Poniedziałek', 
                2 => 'Wtorek', 
                3 => 'Środa', 
                4 => 'Czwartek', 
                5 => 'Piątek', 
                6 => 'Sobota', 
             );

            // Render Table
            if(!empty($employeesData)) {
                foreach($employeesData as $emp) {
                    $empTmp = [
                        'name' => $emp['name'],
                        'id' => $emp['id'],
                        'sum-harvest' => 0,
                        'sum-money' => 0,
                    ];

                    foreach($period as $key => $value) {
                        $day = $value->format('Y-m-d');
                        $header[$day] = [
                            'day' => $day,
                            'name' => $dayNames[$value->format('w')],
                        ];
                        $dayTmp = [];

                        if(!empty($employeesBoxsData[$emp['id']][$day])) {
                            foreach($employeesBoxsData[$emp['id']][$day] as $harvest) {
                                $boxId = $harvest['box_fkey'];

                                if(!empty($dayTmp['harvest'])) {
                                    if(!empty($boxData[$boxId])) {
                                        $dayTmp['harvest'] += ($boxData[$boxId]['weight'] * $harvest['count']);
                                        $dayTmp['money'] += (($boxData[$boxId]['weight'] * $harvest['count']) * $boxData[$boxId]['price']);
                                    }
                                } else {
                                    if(!empty($boxData[$boxId])) {
                                        $dayTmp['harvest'] = $boxData[$boxId]['weight'] * $harvest['count'];
                                        $dayTmp['money'] = (($boxData[$boxId]['weight'] * $harvest['count']) * $boxData[$boxId]['price']);
                                    } else {
                                        $dayTmp['harvest'] = 0;
                                        $dayTmp['money'] = 0;
                                    }
                                }
                            }
                        }  else {
                            $dayTmp['harvest'] = 0;
                            $dayTmp['money'] = 0;

                            if(empty($footer[$day])) {
                                $footer[$day]['harvest'] = 0;
                                $footer[$day]['money'] = 0;
                            }
                        }

                        if(!empty($footer[$day])) {
                            $footer[$day]['harvest'] += $dayTmp['harvest'];
                            $footer[$day]['money'] += $dayTmp['money'];
                        } else {
                            $footer[$day]['harvest'] = $dayTmp['harvest'];
                            $footer[$day]['money'] = $dayTmp['money'];
                        }

                        $empTmp['sum-harvest'] += $dayTmp['harvest'];
                        $empTmp['sum-money'] += $dayTmp['money'];
                        $empTmp['days'][$day] = $dayTmp;
                    }

                    if(!empty( $footer['sum'])) {
                        $footer['sum']['harvest'] += $empTmp['sum-harvest'];
                        $footer['sum']['money'] += $empTmp['sum-money'];
                    } else {
                        $footer['sum']['harvest'] = $empTmp['sum-harvest'];
                        $footer['sum']['money'] = $empTmp['sum-money'];
                    }

                    $result[] = $empTmp;
                }
                $header[] = 'Suma';
            }
        }

        return view('home.index', ['from' => $from, 'to' => $to, 'header' => $header, 'result' => $result, 'footer' => $footer]);
    }

    public function worker(Request $request)
    {
        $from = $request->input('date-from', date('Y-m-d'));
        $to = $request->input('date-to', date('Y-m-d'));

        $employees = new Employees();
        $workers = $employees::where('deleted', 0)->get();

        if(strtotime($from) && strtotime($to)) {
        }

        return view('home.worker', ['from' => $from, 'to' => $to, 'workers' => $workers]);
    }
}
