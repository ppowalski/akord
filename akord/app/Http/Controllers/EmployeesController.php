<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Employees::where('deleted', 0)->get();
        $barcode = new \JFilla\Barcode\BarcodeGeneratorPNG();

        foreach($data as $key => $element) {
            $data[$key]['barcode'] = '<img src="data:image/png;base64,' . base64_encode($barcode->getBarcode($element['code'], $barcode::TYPE_CODE_128, 1, 30, [0, 0, 0], false)) . '">';
            $data[$key]['barcode'] .= '<div style="font-size: 10px;">'. $element['code'] .'</div>';
        }

        return view('employees.index', ['data' => $data]);
    }

    public function downloadCode()
    {
        $data = Employees::where('deleted', 0)->get();
        $barcode = new \JFilla\Barcode\BarcodeGeneratorPNG();

        foreach($data as $key => $element) {

        }
    }

    public function store(Request $request)
    {
        $employeeName = $request->input('employee-name');
        $employeeCode = $request->input('employee-code');

        if(!empty($employeeName) && !empty($employeeCode)) {
            $data = Employees::where('deleted', 0)->where('name', $employeeName)->get();

            if(count($data) > 0) {
                return redirect('employees');
            }

            $employees = new Employees();

            $employees->name = $employeeName;
            $employees->code = $employeeCode;
            $employees->save();
        }

        return redirect('employees');
    }

    public function update(Request $request)
    {
        $employeeId = $request->input('employee-edit-id');
        $employeeName = $request->input('employee-edit-name');
        $employeeCode = $request->input('employee-edit-code');

        if(!empty($employeeId) && !empty($employeeName) && !empty($employeeCode)) {
            $employees = Employees::findOrFail($employeeId);

            $employees->name = $employeeName;
            $employees->code = $employeeCode;
            $employees->save();
        }

        return redirect('employees');
    }

    public function delete(Request $request)
    {
        $employeeId = $request->input('employee-delete-id');

        if(!empty($employeeId)) {
            $employees = Employees::findOrFail($employeeId);

            $employees->deleted = true;
            $employees->save();
        }

        return redirect('employees');
    }
}