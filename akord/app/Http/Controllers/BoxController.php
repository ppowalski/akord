<?php

namespace App\Http\Controllers;

use App\Box;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['']);
    }

    public function index()
    {
        $box = new Box();
        $data = $box::where('deleted', 0)->get();

        return view('box.index', ['data' => $data]);
    }

    public function store(Request $request)
    {
        $boxName = $request->input('box-name');
        $boxWeight = $request->input('box-weight');
        $boxPrice = $request->input('box-price');

        if(!empty($boxName) && !empty($boxWeight) && !empty($boxPrice)) {
            $data = Box::where('deleted', 0)->where('name', $boxName)->get();

            if(count($data) > 0) {
                return redirect('box');
            }

            $box = new Box();

            $box->name = $boxName;
            $box->weight = $boxWeight;
            $box->price = $boxPrice;
            $box->save();
        }

        return redirect('box');
    }

    public function update(Request $request)
    {
        $boxId = $request->input('box-edit-id');
        $boxName = $request->input('box-edit-name');
        $boxWeight = $request->input('box-edit-weight');
        $boxPrice = $request->input('box-edit-price');

        if(!empty($boxId) && !empty($boxName) && !empty($boxWeight) && !empty($boxPrice)) {
            $box = Box::findOrFail($boxId);

            $box->name = $boxName;
            $box->weight = $boxWeight;
            $box->price = $boxPrice;
            $box->save();
        }

        return redirect('box');
    }

    public function delete(Request $request)
    {
        $boxId = $request->input('box-delete-id');

        if(!empty($boxId)) {
            $box = Box::findOrFail($boxId);

            $box->deleted = true;
            $box->save();
        }

        return redirect('box');
    }
}
