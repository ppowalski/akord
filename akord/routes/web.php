<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/worker', 'HomeController@worker');
Route::get('/box', 'BoxController@index');
Route::get('/employees', 'EmployeesController@index')->name('employees');

Route::get('/employee/{id}', 'EmployeeController@index');

Route::post('/employees/add', 'EmployeesController@store');
Route::post('/box/add', 'BoxController@store');

Route::post('/employees/update', 'EmployeesController@update');
Route::post('/box/update', 'BoxController@update');

Route::post('/employees/delete', 'EmployeesController@delete');
Route::post('/box/delete', 'BoxController@delete');

Auth::routes(['register' => false]);
//Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/boxes', 'SynchronizeController@getBoxes');

Route::get('/import', 'ExcelController@index');
Route::post('/excel/revert', 'ExcelController@revert');
Route::post('/excel/delete', 'ExcelController@deleteImport');
Route::post('/excel/import', 'ExcelController@import');


Route::post('/download-excel', 'ExcelController@downloadExcel');