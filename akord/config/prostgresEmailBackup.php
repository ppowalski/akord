<?php

return [
    'pg_exec_command' => 'pg_dump',
//    'pg_exec_command' => 'E:\pg\bin\pg_dump.exe',

    'backup_emails' => [
        'powalski.patryk@gmail.com',
        'akordkania@gmail.com'
    ],

    'email_subject' => env('APP_NAME') . ' backup - ' . date('Y-m-d'),

    'file_name' => "dbbackup_". date('Y_m_d_H_i_s') .".sql",
];
