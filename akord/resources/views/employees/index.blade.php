@extends('template')
@section('title', 'Pracownicy')

@section('content')
    <div class="mt-3"></div>
    <div class="row">
        <div class="form-group col-sm-0 col-md-9 col-lg-10">
        </div>
        <div class="form-group col-md-3 col-lg-2">
            <label style="height: 19px;"></label>
            <button type="submit" class="btn btn-primary w-100 float-right" data-toggle="modal" data-target="#addEmployees">Dodaj pracownika</button>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="table-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Pracownik</th>
                <th scope="col">Numer kodu</th>
                <th scope="col">Kod kreskowy</th>
                <th scope="col">Akcje</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $user)
                <tr>
                    <td>{{$user['id']}}</td>
                    <td><a href="/employee/{{$user['id']}}">{{$user['name']}}</a></td>
                    <td>{{$user['code']}}</td>
                    <td>{!! $user['barcode'] !!}</td>
                    <td>
                        <button class="btn btn-primary btn-sm mr-2 modal-box" data-toggle="modal" data-target="#updateEmployees" data-action="updateEMP" data-emp_id="{{$user['id']}}" data-emp_name="{{$user['name']}}" data-emp_code="{{$user['code']}}">Edytuj</button>
                        <button class="btn btn-danger btn-sm modal-box" data-toggle="modal" data-target="#deleteEmployees" data-action="deleteEMP" data-emp_id="{{$user['id']}}">Usuń</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- CREATE NEW -->
    <div class="modal fade" id="addEmployees" tabindex="-1" role="dialog" aria-labelledby="addEmployeesLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/employees/add">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="addEmployeesLabel">Dodawanie pracownika</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="employee-name">Nazwa pracownika</label>
                            <input type="text" class="form-control" id="employee-name" name="employee-name">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="employee-code">Numer kodu</label>
                            <input type="text" class="form-control" id="employee-code" name="employee-code">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- UPDATE -->
    <div class="modal fade" id="updateEmployees" tabindex="-1" role="dialog" aria-labelledby="updateEmployeesLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/employees/update">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateEmployeesLabel">Edycja pracownika</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="employee-edit-id" name="employee-edit-id">

                            <label for="employee-edit-name">Nazwa pracownika</label>
                            <input type="text" class="form-control" id="employee-edit-name" name="employee-edit-name">
                        </div>
                        <div class="form-group">
                            <label for="employee-edit-code">Numer kodu</label>
                            <input type="text" class="form-control" id="employee-edit-code" name="employee-edit-code">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary">Aktualizuj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- DELETE -->
    <div class="modal fade" id="deleteEmployees" tabindex="-1" role="dialog" aria-labelledby="deleteEmployeesLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/employees/delete">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteEmployeesLabel">Usuwanie pracownika</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="employee-delete-id" name="employee-delete-id">
                        Czy na pewno chcesz usunąć pracownika?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-danger">Usuń</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
