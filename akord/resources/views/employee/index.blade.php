@extends('template')
@section('title', 'Pracownik')

@section('content')
<div class="row">
    <div class="col-12 mt-3">
        <h3>Pracownik <br>{{$employee['name']}}</h3>
    </div>
</div>

<div class="mt-3"></div>
<form>
    <div class="row">
        <div class="form-group col-md-5">
            <label for="date-from">Okres od</label>
            <input type="date" class="form-control datepicker" id="date-from" name="date-from" value="{{ $from }}">
        </div>
        <div class="form-group col-md-5">
            <label for="date-from">Okres do</label>
            <input type="date" class="form-control datepicker" id="date-to" name="date-to" value="{{ $to }}">
        </div>

        <div class="form-group col-md-2">
            <label style="height: 19px;"></label>
            <button type="submit" class="btn btn-primary w-100">Filtruj</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="table-responsive" id="table-scroll">
        <table class="table table-striped">
            <thead class="table-dark">
                <tr style="text-align: center;">
                    <th class="col-sticky"></th>
                    @foreach($header as $key => $day)
                        <th style="white-space: nowrap;">{{ $day['date'] }} <br> {{ $day['dayName'] }} </th>
                    @endforeach
                    <th>Całość</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $boxName => $row)
                    <tr style="text-align: center;">
                        <td class="col-sticky">{{ $row['name'] }}</td>
                        @foreach($header as $day => $element)
                            @if(!empty($row[$day]))
                                <td style="white-space: nowrap;">
                                    {{ $row[$day]['count'] }} <br> {{ $row[$day]['kg'] }} kg <br> {{ number_format($row[$day]['price'], 2) }} PLN
                                </td>
                            @else
                                <td>-</td>    
                            @endif
                        @endforeach
                        <td style="white-space: nowrap;">
                            {{ $row['sum']['count'] }} <br> {{ $row['sum']['kg'] }} kg <br> {{ number_format($row['sum']['price'], 2) }} PLN
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="table-dark">
                <tr style="text-align: center;">
                    <th class="col-sticky">Podsumowanie</th>
                    @foreach($footer as $day => $tfoot)
                        @if($day === 'sum')
                            @continue
                        @endif 

                        <th style="white-space: nowrap;">
                        {{ $tfoot['count'] }} <br> {{ $tfoot['kg'] }} kg <br> {{ number_format($tfoot['price'], 2) }} PLN
                        </th>
                    @endforeach
                    <th style="white-space: nowrap;">
                        {{ $footer['sum']['count'] }} <br> {{ $footer['sum']['kg'] }} kg <br> {{ number_format($footer['sum']['price'], 2) }} PLN
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="col-12 mt-2 text-right">
        <button id="download-excel" type="button" class="btn btn-primary">Pobierz excel</button>
    </div>
</div>
@endsection


@section('script')
<script>
    function redirectPost(url, data) {
        var form = document.createElement('form');
        document.body.appendChild(form);
        form.method = 'post';
        form.action = url;
        for (var name in data) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = name;
            input.value = data[name];
            form.appendChild(input);
        }
        form.submit();
    }

    $(function() {
        $('#download-excel').click(function() {
            let table = $('.table-responsive').html();

            redirectPost('/download-excel', { _token: "{{ csrf_token() }}", workerName: "{{ $employee['name'] }}", table: encodeURIComponent(table) });
        });
    });
</script>
@endsection