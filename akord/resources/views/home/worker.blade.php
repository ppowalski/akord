@extends('template')
@section('title', 'Strona główna')

@section('content')
    <div class="mt-3"></div>
    <form>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="date-from">Okres od</label>
                <input type="date" class="form-control datepicker" id="date-from" name="date-from" value="{{$from}}">
            </div>
            <div class="form-group col-md-3">
                <label for="date-from">Okres do</label>
                <input type="date" class="form-control datepicker" id="date-to" name="date-to" value="{{$to}}">
            </div>
            <div class="form-group col-md-3">
                <label for="date-from">Pracownik</label>
                <select class="form-control" id="worker" name="worker">
                    @foreach($workers as $worker)
                        <option value="{{$worker['id']}}">{{$worker['name']}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-3">
                <label style="height: 19px;"></label>
                <button type="submit" class="btn btn-primary w-100">Filtruj</button>
            </div>
        </div>
    </form>
@endsection
