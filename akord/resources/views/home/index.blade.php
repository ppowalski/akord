@extends('template')
@section('title', 'Strona główna')

@section('content')
    <div class="mt-3"></div>
    <form>
        <div class="row">
            <div class="form-group col-md-5">
                <label for="date-from">Okres od</label>
                <input type="date" class="form-control datepicker" id="date-from" name="date-from" value="{{$from}}">
            </div>
            <div class="form-group col-md-5">
                <label for="date-from">Okres do</label>
                <input type="date" class="form-control datepicker" id="date-to" name="date-to" value="{{$to}}">
            </div>

            <div class="form-group col-md-2">
                <label style="height: 19px;"></label>
                <button type="submit" class="btn btn-primary w-100">Filtruj</button>
            </div>
        </div>
    </form>

    <div class="table-responsive" id="table-scroll">
        <table class="table table-striped">
            <thead class="table-dark">
                <tr style="text-align: center;">
                    @foreach($header as $key => $element)
			@if($key === 0)
				<th scope="col" class="min-150 col-sticky">
                    @if(is_array($element))
                        {{$element['day']}} <br> {{ $element['name'] }}
                    @else
                        {{ $element }}
                    @endif
                </th>
			@else
                        	<th scope="col" class="min-150">
                                @if(is_array($element))
                                    {{$element['day']}} <br> {{ $element['name'] }}
                                @else
                                    {{ $element }}
                                @endif
                            </th>
                    	@endif
		    @endforeach
                </tr>
            </thead>
            <tbody>
            @foreach($result as $key => $emp)
                <tr style="text-align: center;">
		    <td class="col-sticky"><a href="/employee/{{$emp['id']}}">{{$emp['name']}}</a></td>
                    @foreach($emp['days'] as $day)
                        @if($day['harvest'] == 0)
                            <td>-</td>
                        @else
                            <td style="white-space: nowrap;">{{$day['harvest']}} KG ({{ number_format($day['money'], 2) }} PLN)</td>
                        @endif
                    @endforeach
                    @if($emp['sum-harvest'] == 0)
                        <td>-</td>
                    @else
                        <td style="white-space: nowrap;">{{$emp['sum-harvest']}} KG ({{ number_format($emp['sum-money'], 2) }} PLN)</td>
                    @endif
                </tr>
            @endforeach
            <tfoot class="table-dark">
            <tr style="text-align: center;">
                <th scope="col" class="col-sticky">Suma</th>
                @foreach($footer as $key => $element)
                    @if($key == 'sum')
                        @continue
                    @endif

                    @if($element['harvest'] == 0)
                        <td>-</td>
                    @else
                        <th scope="col" style="white-space: nowrap;">{{$element['harvest']}} KG ({{ number_format($element['money'], 2) }} PLN)</th>
                    @endif

                @endforeach
                @isset($footer['sum']['harvest'])
                    @if($footer['sum']['harvest'] == 0)
                        <th scope="col">-</th>
                    @else
                        <th scope="col" style="white-space: nowrap;">{{$footer['sum']['harvest']}} KG ({{ number_format($footer['sum']['money'], 2) }} PLN)</th>
                    @endif
                @endisset
            </tr>
            </tfoot>
        </table>
    </div>
@endsection
