@extends('template')
@section('title', 'Pracownicy')

@section('content')
    <div class="mt-3"></div>

    @if (\Session::has('success'))
        <div class="alert alert-success">
            {!! \Session::get('success') !!}
        </div>
    @endif

    @if (\Session::has('error'))
        <div class="alert alert-danger">
            {!! \Session::get('error') !!}
        </div>
    @endif

    <form method="POST" action="/excel/import" enctype="multipart/form-data">
        <div class="row">
            <h3 class="col-12 mb-3">Import zebranych skrzynek</h3>
            @csrf
            <div class="form-group col-12">
                <label for="box">Skrzynka</label>
                <select class="form-control" id="box" name="box">
                    @foreach($data as $id => $box)
                        <option value="{{$box['id']}}">{{$box['name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-12">
                <label for="day">Dzień</label>
                <input type="date" class="form-control" id="day" name="day">
            </div>
            <div class="form-group col-12">
                <label for="file">Plik</label>
                <input type="file" class="form-control" id="file" name="file" accept="application/vnd.ms-excel">
            </div>

            <div class="form-group col-md-2 offset-md-10">
                <label style="height: 19px;"></label>
                <button type="submit" class="btn btn-primary w-100 float-right">Importuj</button>
            </div>
        </div>
    </form>

    <div class="row">
        <h3 class="col-12 mb-3">Historia importów</h3>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="table-dark">
            <tr>
                <th scope="col" class="min-150">ID</th>
                <th scope="col" class="min-150">Nazwa pliku</th>
                <th scope="col" class="min-150">Skrzynka</th>
                <th scope="col" class="min-150">Data zbioru</th>
                <th scope="col" class="min-150">Data importu</th>
                <th scope="col" class="min-150">Akcje</th>
            </tr>
            </thead>
            <tbody>
            @foreach($backupData as $backup)
                @if($backup['deleted'])
                <tr style="background-color: #e6a8a8;">
                @else
                <tr style="background-color: #a2dab8;">
                @endif
                    <td>{{$backup['id']}}</td>
                    <td>{{$backup['name']}}</td>
                    <td>{{$backup['box_name']}}</td>
                    <td>{{$backup['date']}}</td>
                    <td>{{$backup['created_at']}}</td>

                    @if($backup['deleted'])
                        <td><button class="btn btn-sm btn-success modal-box" data-toggle="modal" data-target="#revertImport" data-action="revertImport" data-backup_id="{{$backup['id']}}">Przywróć</button></td>
                    @else
                        <td><button class="btn btn-sm btn-danger modal-box" data-toggle="modal" data-target="#deleteImport" data-action="deleteImport" data-backup_id="{{$backup['id']}}">Wycofaj</button></td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>

    <!-- Revert -->
    <div class="modal fade" id="deleteImport" tabindex="-1" role="dialog" aria-labelledby="deleteImportLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/excel/delete">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteImportLabel">Wycofywanie importu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="delete-id" name="delete-id" value="0">
                        Czy na pewno chcesz wycofać import?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-danger">Wycofaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Restore -->
    <div class="modal fade" id="revertImport" tabindex="-1" role="dialog" aria-labelledby="revertImportLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/excel/revert">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="revertImportLabel">Przywracanie importu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="revert-id" name="revert-id" value="0">
                        Czy na pewno chcesz przywrócić import?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-success">Przywróć</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
