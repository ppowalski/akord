@extends('template')
@section('title', 'Skrzynki')

@section('content')
    <div class="mt-3"></div>
    <div class="row">
        <div class="form-group col-sm-0 col-md-9 col-lg-10">
        </div>
        <div class="form-group col-md-3 col-lg-2">
            <label style="height: 19px;"></label>
            <button type="submit" class="btn btn-primary w-100 float-right" data-toggle="modal" data-target="#addBox">Dodaj skrzynkę</button>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="table-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nazwa skrzynki</th>
                <th scope="col">Waga skrzynki (KG)</th>
                <th scope="col">Cena za kilogram (zł)</th>
                <th scope="col">Akcje</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $box)
                <tr>
                    <td>{{$box['id']}}</td>
                    <td>{{$box['name']}}</td>
                    <td>{{$box['weight']}}</td>
                    <td>{{$box['price']}}</td>
                    <td>
                        <button class="btn btn-primary btn-sm mr-2 modal-box" data-toggle="modal" data-target="#updateBox" data-action="updateBOX" data-box_id="{{$box['id']}}" data-box_name="{{$box['name']}}" data-box_weight="{{$box['weight']}}" data-box_price="{{$box['price']}}">Edytuj</button>
                        <button class="btn btn-danger btn-sm modal-box" data-toggle="modal" data-target="#deleteBox" data-action="deleteBOX" data-box_id="{{$box['id']}}">Usuń</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <!-- ADD BOX -->
    <div class="modal fade" id="addBox" tabindex="-1" role="dialog" aria-labelledby="addBoxLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/box/add">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="addBoxLabel">Dodawanie skrzynki</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="box-name">Nazwa skrzynki</label>
                            <input type="text" class="form-control" id="box-name" name="box-name">
                        </div>
                        <div class="form-group">
                            <label for="box-weight">Waga skrzynki (KG)</label>
                            <input type="number" class="form-control" id="box-weight" name="box-weight" min="0" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="box-weight">Cena za kilogram (zł)</label>
                            <input type="number" class="form-control" id="box-price" name="box-price" min="0" step="0.01">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- UPDATE BOX -->
    <div class="modal fade" id="updateBox" tabindex="-1" role="dialog" aria-labelledby="updateBoxLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/box/update">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateBoxLabel">Edycja skrzynki</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="box-edit-id" name="box-edit-id">
                        <div class="form-group">
                            <label for="box-name">Nazwa skrzynki</label>
                            <input type="text" class="form-control" id="box-edit-name" name="box-edit-name">
                        </div>
                        <div class="form-group">
                            <label for="box-weight">Waga skrzynki (KG)</label>
                            <input type="number" class="form-control" id="box-edit-weight" name="box-edit-weight" min="0" step="0.01">
                        </div>
                        <div class="form-group">
                            <label for="box-price">Cena za kilogram (zł)</label>
                            <input type="number" class="form-control" id="box-edit-price" name="box-edit-price" min="0" step="0.01">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary">Aktualizuj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- ADD BOX -->
    <div class="modal fade" id="deleteBox" tabindex="-1" role="dialog" aria-labelledby="deleteBoxLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content text-white" style="background-color: #262b2f">
                <form method="POST" action="/box/delete">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteBoxLabel">Usuwanie skrzynki</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #fff; text-shadow: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="box-delete-id" name="box-delete-id">
                        Czy na pewno chcesz usunąć skrzynkę?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-danger">Usuń</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
