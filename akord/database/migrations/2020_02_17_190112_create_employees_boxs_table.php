<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesBoxsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_boxs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employee_fkey');
            $table->bigInteger('box_fkey');
            $table->bigInteger('count');
            $table->date('date');
            $table->boolean('deleted')->default(false);
            $table->timestamps();

//            $table->foreign('employee_fkey')->references('id')->on('employees');
//            $table->foreign('box_fkey')->references('id')->on('boxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_boxs');
    }
}
